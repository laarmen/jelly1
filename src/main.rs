use std::io;

fn parse_line(line: &str) -> anyhow::Result<Vec<bool>> {
    line.trim_end()
        .chars()
        .map(|c| match c {
            '1' => Ok(true),
            '0' => Ok(false),
            _ => Err(anyhow::anyhow!("unknown char: {}", c)),
        })
        .collect()
}

fn main() -> anyhow::Result<()> {
    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line)?;
    let mut start = parse_line(&input_line)?;
    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line)?;
    let target = parse_line(&input_line)?;
    anyhow::ensure!(
        start.len() == target.len(),
        "The length of both strings doesn't match"
    );
    println!("{}", compute_flipcount(&mut start, &target));
    Ok(())
}

/// Compute the minimum number of bitflips needed under the weird rules given as an exercise to get
/// from the start state to the target state. As this is an exercise without much context we allow
/// ourselves to mutate the input state directly instead of cloning it.
///
/// It can be proven via recursion that this algorithm will give the minimum amount of flips
/// needed. Note that the algorithm itself might *not* be the most optimal, see the comments within
/// for possible optimizations. This version was written to match a math proof, hence some
/// suboptimal choices within the context of Rust such as recursion. The complexity of the
/// algorithm seems to be O(N^3), but that's just an educated guess. It's at least O(N^2).
///
/// Possible optimizations might be specialized versions of the set_next_* functions when we know
/// the start buffer to be in a given state (10+ or 0+) which we might be able to find in constant
/// time as they probably depend solely on the length of the buffer, but the current code is
/// already non-trivial to understand as is, and the performance seem reasonable (10ms for N=25 on
/// a debug build). Such optimization could bring us down to O(N) for the general algorithm.
///
/// In case of memory constraints, especially on the stack, one might want to rewrite the various
/// functions to use loops instead of recursion, as we're not tail-call optimization compatible
/// (and Rust didn't support it last I checked).
///
/// If it's not possible to find these const-time set_next variants, or if the constant cost is too
/// high, we might want to distribute the load on a thread pool (rayon ?), by cloning the buffer
/// instead of mutating it. The fixed costs of such a setup might be higher than the potential
/// gains, though.
fn compute_flipcount(start: &mut [bool], target: &[bool]) -> usize {
    assert!(start.len() == target.len());
    match target {
        [val, target @ ..] => {
            let this_bit = if *val {
                set_next_true(start)
            } else {
                set_next_false(start)
            };
            this_bit
                + match start {
                    [_, start @ ..] => compute_flipcount(start, target),
                    _ => unreachable!(),
                }
        }
        [] => 0,
    }
}

/// Internal function.
///
/// Prepare a tail buffer so that the boolean in front of it can be flipped.
/// Note that this is the tail buffer, the flipped boolean isn't supposed to
/// be in it.
fn prepare_flip(state: &mut [bool]) -> usize {
    match state {
        [_, ..] => {
            set_next_true(state)
                + if let [_, r @ ..] = state {
                    set_all_false(r)
                } else {
                    0
                }
        }
        [] => 0,
    }
}

/// Mutate a buffer to set its first element to true.
///
/// Returns the number of flip operations necessary under the exercise rules.
fn set_next_true(state: &mut [bool]) -> usize {
    match state {
        [] => 0,
        [bit @ false] => {
            *bit = true;
            1
        }
        [true, ..] => 0,
        [b1 @ false, b2 @ false] => {
            *b1 = true;
            *b2 = true;
            2
        }
        [bit @ false, rest @ ..] => {
            *bit = true;
            1 + prepare_flip(rest)
        }
    }
}

/// Mutate a given buffer to set its first element to false.
///
/// Returns the number of bitflip necessary for this operation.
fn set_next_false(state: &mut [bool]) -> usize {
    match state {
        [] => 0,
        [false, ..] => 0,
        [bit @ true, rest @ ..] => {
            *bit = false;
            1 + prepare_flip(rest)
        }
    }
}

/// Mutate a buffer to set all its elements to false.
///
/// Returns the number of bitflips necessary.
fn set_all_false(state: &mut [bool]) -> usize {
    match state {
        [] => 0,
        [false, rest @ ..] => set_all_false(rest),
        [bit @ true, rest @ ..] => {
            *bit = false;
            set_next_true(rest)
                + if let [_, rest @ ..] = rest {
                    set_all_false(rest) + 1
                } else {
                    1
                }
                + set_all_false(rest)
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn is_flippable(state: &[bool]) -> bool {
        match state {
            [] => true,
            [_] => true,
            [_, v, rest @ ..] => rest.iter().fold(*v, |acc, elt| acc && !elt),
        }
    }

    #[test]
    fn example_1() {
        let mut start = vec![true, true, false, true];
        assert_eq!(
            compute_flipcount(&mut start, &[false, true, false, false]),
            2
        )
    }

    #[test]
    fn example_2() {
        let mut start = vec![true, false, true, false, true, false];
        assert_eq!(
            compute_flipcount(&mut start, &[false, true, false, true, false, true]),
            26
        )
    }

    #[test]
    fn example_3() {
        let mut start = vec![
            true, true, false, false, true, false, false, true, false, false, false,
        ];
        assert_eq!(
            compute_flipcount(
                &mut start,
                &[true, false, false, false, false, true, true, false, false, true, true]
            ),
            877
        )
    }

    #[test]
    fn test_set_all_false_nop_on_all_false() {
        let mut buf: Vec<bool> = std::iter::repeat(false).take(25).collect();
        assert_eq!(set_all_false(&mut buf), 0);
        for v in buf {
            assert!(!v);
        }
    }

    #[test]
    fn test_set_next_true_is_nop_on_true() {
        let mut buf = vec![
            true, true, false, false, true, false, false, true, false, false, false,
        ];
        let other = buf.clone();
        assert_eq!(set_next_true(&mut buf), 0);
        assert_eq!(&buf, &other);
    }

    #[test]
    fn test_prepare_flip() {
        let mut buf = vec![false, false, false];
        assert_eq!(prepare_flip(&mut buf[1..]), 3);
        assert!(is_flippable(&buf));
    }

    #[test]
    fn set_next_true_when_prepared() {
        let mut buf = vec![false, false, true];
        prepare_flip(&mut buf[1..]);
        assert!(is_flippable(&buf));

        assert_eq!(set_next_true(&mut buf), 1);
        assert!(buf[0]);
        assert!(is_flippable(&buf));
    }

    #[test]
    fn set_next_false_when_prepared() {
        let mut buf = vec![true, false, false];
        prepare_flip(&mut buf[1..]);
        assert!(is_flippable(&buf));

        assert_eq!(set_next_false(&mut buf), 1);
        assert!(!buf[0]);
        assert!(is_flippable(&buf));
    }
}
